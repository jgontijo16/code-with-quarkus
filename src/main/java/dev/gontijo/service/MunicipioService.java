package dev.gontijo.service;

import javax.inject.Inject;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import dev.gontijo.exception.NameException;
import dev.gontijo.model.Municipio;
import dev.gontijo.repository.MunicipioRepository;

@Service
public class MunicipioService {
	
	@Inject
	MunicipioRepository repository;
	
	public Page<Municipio> listar(int page, int size){
		Pageable pageable = PageRequest.of(page, size);
		return repository.findAll(pageable);
	}
	
	public Page<Municipio> listarPorEstado(int page, int size, Long id){
		Pageable pageable = PageRequest.of(page, size);
		return repository.findByEstadoId(pageable, id);
	}

	public Page<Municipio> listarPorNome(int page, int size, String name) throws Exception {
		Pageable pageable = PageRequest.of(page, size);
		System.out.println(name);
		if(name.length() < 3) {
			throw new NameException("Nome deve ter conter 3 ou mais caracteres");
		}
		return repository.findByNameContaining(pageable, name);
	}
	
	public Long contarMunicipios() {
		return repository.count();
	}
	
	public Municipio salvar(Municipio municipio) {
		return repository.save(municipio);
	}
	
	public void deletar(Long id) {
		repository.deleteById(id);
	}
}
