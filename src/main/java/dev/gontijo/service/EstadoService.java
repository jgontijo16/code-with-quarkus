package dev.gontijo.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import dev.gontijo.model.Estado;
import dev.gontijo.repository.EstadoRepository;

@Service
public class EstadoService {
	
	@Autowired
	EstadoRepository repository;
	
	public Estado salvar(Estado estado) {
		return repository.save(estado);
	}
	
	public void deletar(Long id) {
		repository.deleteById(id);
	}
	
	public Page<Estado> listar(int size, int page){
		Pageable pageable = PageRequest.of(size, page);
		return repository.findAll(pageable);
	}
}
