package dev.gontijo.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import dev.gontijo.model.Estado;

@Repository
public interface EstadoRepository extends org.springframework.data.repository.Repository<Estado, Long>{
	
	Page<Estado> findAll(Pageable page);

	Estado save(Estado estado);

	void deleteById(Long id);

	Estado findById(Long id);
}
