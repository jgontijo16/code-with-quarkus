package dev.gontijo.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import dev.gontijo.model.Municipio;

@Repository
public interface MunicipioRepository extends org.springframework.data.repository.Repository<Municipio, Long>{

	Page<Municipio> findAll(Pageable pageable);

	Page<Municipio> findByEstadoId(Pageable pageable, Long id);

	Page<Municipio> findByNameContaining(Pageable pageable, String name);

	void deleteById(Long id);

	Municipio save(Municipio municipio);

	@Query("SELECT COUNT(m) FROM Municipio m")
	Long count();

}
