package dev.gontijo.controller;

import javax.inject.Inject;
import javax.ws.rs.DefaultValue;

import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import dev.gontijo.model.Estado;
import dev.gontijo.service.EstadoService;

@RestController
@RequestMapping("/estados")
public class EstadoController {
	
	@Inject
	EstadoService service;
	
	@GetMapping
	public Page<Estado> lista(
			@RequestParam(value = "size", defaultValue = "5") int size,
			@RequestParam(value = "page", defaultValue = "0") int page){
		return service.listar(page, size);
	}
	
	@PostMapping
	public Estado post(@RequestBody Estado estado) {
		return service.salvar(estado);
	}
	
	@PutMapping
	public Estado put(@RequestBody Estado estado) {
		return service.salvar(estado);
	}
	
	@DeleteMapping
	public void delete(@RequestBody Long id) {
		service.deletar(id);
	}
}
