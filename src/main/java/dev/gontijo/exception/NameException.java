package dev.gontijo.exception;

import java.io.Serializable;

public class NameException extends Exception implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NameException() {
		super();
	}
	
	public NameException(String msg) {
		super(msg);
	}
	
	public NameException(String msg, Exception e) {
		super(msg, e);
	}
}
