package dev.gontijo.exception;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.ExceptionMapper;

public class NameExceptionHandler implements ExceptionMapper<NameException> {

	@Override
	public Response toResponse(NameException exception) {
		return Response.status(Status.BAD_REQUEST).entity(exception.getMessage()).build();
	}

	
}
