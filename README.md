# Teste para Desenvolvedor

[![e-Precise Logo](https://www.e-precise.com.br/assets/images/logo_com_sombra.png)](https://www.e-precise.com.br/)

### Requisitos

- Quarkus (https://quarkus.io/)
- Linguagem: Java
- Estilo Arquitetural: REST
- Inserção de registros em banco de dados SQL

## Detalhes do teste

### Critérios de avaliação

- Arquitetura do projeto (DDD ou MVC)
- Aplicação de orientação a objetos
- Funcionalidades e funcionamento

### Funcionalidades

A API contém as seguintes funcionalidades:

1. Estados: GET (pesquisa com paginação)/ POST (inserir) / PUT (atualizar) / DELETE (remover)
2. Estados: GET número de registros
3. Cidades: GET (pesquisa com paginação)/ POST (inserir) / PUT (atualizar) / DELETE (remover)
4. Cidades: GET por estado (pesquisa com paginação)
5. Cidades: GET por nome (pesquisa com paginação, contendo o nome a partir de 3 caracteres)
6. Cidades: GET número de registros

### Especificações técnicas

- Criar um fork do projeto (X)
- Descrever suas facilidades e dificuldades encontradas no processo de desenvolvimento.   
    - Tive problemas com a importação do projeto após fazer um fork e clone. Acredito por causa do versionamento das dependências
    - Adaptação ao Quarkus, demorei um pouco para compreender a documentação e as dependências
    - Quando descobri sobre as dependências Spring, tive facilidade para implementar as funcionalidades
    - Com problemas com as requisições pelo Postman, ativei a Swagger-UI para documentação e teste dos endpoints. Localizada em http://localhost:8080/swagger-ui
    - Com esse projeto, tive o primeiro contato com Bitbucket e também com Docker, para container automático da aplicação e banco de dados.
    - Mesmo que eu seja desclassificado por conta da questão do fork do repositório, aprecio a oportunidade e a experiência de ter conhecido Quarkus e poder ter tido esse desafio de programação.

## Pré-configurações (informativo)

### Rodando a aplicação em modo dev

Você pode rodar sua aplicação em modo desenvolvimento (que permite live coding), usando:

```
./mvnw quarkus:dev
```

### Empacotando e rodando a aplicação

A aplicação pode ser empacotada usando `./mvnw package`.
O artefato resultante do empacotamento é o arquivo `code-with-quarkus-dev.jar` no diretório `/target`.
Esteja ciente que não é gerado um _über-jar_, as dependências são copiadas no diretório `target/lib`.

A aplicação pode ser executada usando `java -jar target/code-with-quarkus-dev.jar`.

## Contato e Informações

### Dúvidas?
`contato@eprecise.com.br` com título `Teste para Desenvolvedor - Dúvida`

### Envio da prova?
`contato@eprecise.com.br` com título `Teste para Desenvolvedor - Finalização`. 
Incluir link com fork do repositório. 

### Prazo?
1 semana após o recebimento do teste.
